import time
import dummy_threading as threading
import _thread
sem = threading.Lock()
contador = 0
def thread1():
    while True:
        sem.acquire()
        contador = contador + 1
        print("Thread1:"+ str(contador))
        sem.release() 
def thread2():
        sem.acquire()
        contador = contador + 1
        print("Thread2:"+  str(contador))
        sem.release()
_thread.start_new_thread(thread1,())
_thread.start_new_thread(thread2,())

